#include "NtpTime.h"
#include <time.h>


#define SSID    "My-SSID"
#define PW      "SuperSecretPassword"

// Update through NTP in seconds
const unsigned int UPDATE_INTERVAL = 60*5;
// Receive NTP packet timeout in ms
const unsigned int RECEIVE_TIMOUT = 1500;
// NTP pool host
const std::string NTP_SERVER_NAME = "0.de.pool.ntp.org";//"0.de.pool.ntp.org";


unsigned int counter = 0;
void setup() {
    // put your setup code here, to run once:

    Serial.begin(115200);
    Serial.println("Connecting to WiFi");
    connectToWifi();
 
    // Initialize NtpTime module
    if(!NTPTime.init(UPDATE_INTERVAL, RECEIVE_TIMOUT, NTP_SERVER_NAME /*, port=2390*/)){
        Serial.println("NtpTime initialization failed!");
    }

}


void connectToWifi() {
  WiFi.begin(SSID, PW);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(300);
  }
  Serial.println();
}

void printTime(){
    // Get current UTC time
    tm utc = NTPTime.getUTC();
    // Change to CEST time
    utc.tm_hour += 2;
    // Format to string and print
    Serial.println(">>> UTC: " + String(asctime(&utc)));
}

void loop() {
    // Update isntance every cycle - this does not update through NTP service
    NTPTime.update();

    // Print time every second
    if(counter%10 == 0)
        printTime();
    delay(100);
    counter++;
}
