#ifndef NtpTime_H
#define NtpTime_H

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <string>
#include <time.h>


class NtpTime{

public:
    NtpTime();
    bool init(unsigned int update_interval_s=0, unsigned int recv_timout_ms=2000, 
        std::string server_name="time.nist.gov", unsigned short port=2390);
    void update();

    bool getNtpHost(std::string server_name="");

    time_t getTimestamp() const;
    tm getUTC() const;
    bool isSynced() const;

    void setUpdateInterval(unsigned int interval);
    void setReceiveTimeout(unsigned short timeout);

protected:
    bool sendNtpPacket();
    bool readNtpPacket();
    void resetMembers();

    WiFiUDP udp;
    static const unsigned short BYTE_LEN = 48;
    byte packet_buffer[BYTE_LEN];
    IPAddress address;
    std::string server_name;
    time_t last_timestamp, last_read;
    unsigned int update_interval_s, last_updated, receive_timeout_ms, read_started;
    bool is_updating, is_ntp_sent, is_synced;
};

extern NtpTime NTPTime;

#endif
