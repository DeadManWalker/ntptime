#include "NtpTime.h"


NtpTime::NtpTime()
 :server_name(""), last_timestamp(0), last_read(0), update_interval_s(0),
  last_updated(0), receive_timeout_ms(0), is_updating(true), is_ntp_sent(false),
  read_started(0), is_synced(false){
}

bool NtpTime::init(unsigned int _update_interval_s, unsigned int recv_timout_ms,
                std::string _server_name, unsigned short port){
    if(!getNtpHost(_server_name))
        return false;
    receive_timeout_ms = recv_timout_ms;
    update_interval_s = _update_interval_s; 
    return udp.begin(port);
}
void NtpTime::update(){
    if(!is_updating || update_interval_s == 0){
        time_t current = millis();
        if((current-last_updated)/1000 > update_interval_s){
            is_updating = true;
        }
        return;
    }

    if(!is_ntp_sent){
        if(!sendNtpPacket()){
            return;}
        is_ntp_sent = true;
        return;
    }
    if(read_started > 0){
        if((millis()-read_started) > receive_timeout_ms){
            resetMembers();
            is_updating = true;
            return;
        }
    }else{
        read_started = millis();
    }
    if(readNtpPacket()){
        resetMembers();
        is_synced = true;
        last_updated = millis();
    }
}
time_t NtpTime::getTimestamp() const{
    return last_timestamp + ( (millis()-last_read) / 1000);
}
tm NtpTime::getUTC() const{
    time_t ts = getTimestamp();
    return *(gmtime(&ts));
}

bool NtpTime::isSynced() const{
    return is_synced;
}

bool NtpTime::sendNtpPacket(){
    memset(packet_buffer, 0, BYTE_LEN);
    packet_buffer[0] = 0b11100011;   // LI, Version, Mode
    packet_buffer[1] = 0;     // Stratum, or type of clock
    packet_buffer[2] = 6;     // Polling Interval
    packet_buffer[3] = 0xEC;  // Peer Clock Precision
    packet_buffer[12]  = 49;
    packet_buffer[13]  = 0x4E;
    packet_buffer[14]  = 49;
    packet_buffer[15]  = 52;
    static const unsigned short NTP_PORT = 123;
    if(!udp.beginPacket(address, NTP_PORT))
        return false;
    udp.write(packet_buffer, BYTE_LEN);
    if(!udp.endPacket())
        return false;
    return true;
}

bool NtpTime::readNtpPacket(){
    if(!udp.parsePacket())
        return false;

    udp.read(packet_buffer, BYTE_LEN);
    unsigned long high_word = word(packet_buffer[40], packet_buffer[41]);
    unsigned long low_word = word(packet_buffer[42], packet_buffer[43]);
    unsigned long secs_since_1900 = high_word << 16 | low_word;
    static const unsigned long SEVENTY_YEARS = 2208988800UL;
    last_timestamp = secs_since_1900 - SEVENTY_YEARS; //Unix time
    last_read = millis();
    return true;
}

void NtpTime::resetMembers(){
    is_updating = false;
    is_ntp_sent = false;
    read_started = 0;
}

bool NtpTime::getNtpHost(std::string _server_name){
    if(_server_name.length() > 0)
        server_name = _server_name;
    return WiFi.hostByName(server_name.c_str(), address);
}

void NtpTime::setUpdateInterval(unsigned int interval){
    update_interval_s = interval;
}

NtpTime NTPTime;
